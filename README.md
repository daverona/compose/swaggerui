# daverona/compose/swagger

## Prerequisites

* `swaggerui.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/docker-compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/docker-compose/traefik) out

## Quick Start

```bash
cp .env.example .env  
# edit .env
docker-compose up --detach
```

Visit [http://swaggerui.example](http://swaggerui.example).

## References

* Swagger UI: [https://swagger.io/docs/open-source-tools/swagger-ui/](https://swagger.io/docs/open-source-tools/swagger-ui/)
* Swagger UI repository: [https://github.com/swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui)
* Swagger UI registry: [https://hub.docker.com/r/swaggerapi/swagger-ui/](https://hub.docker.com/r/swaggerapi/swagger-ui/)
